﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;


namespace PhotoPublish
{
    class Program
    {
        enum Action { Copy, Convert, None }

        class Item
        {
            public FileInfo src;
            public FileInfo dst;
            public Action action;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Source directory:");
            var srcdir = Console.ReadLine();

            Console.WriteLine();
            Console.WriteLine("Target directory:");
            var targetdir = Console.ReadLine();

            Console.WriteLine();
            var files = new List<Item>();
            var dirs = new List<string> { srcdir };
            while (dirs.Count > 0)
            {
                var curdir = dirs[dirs.Count - 1];
                dirs.RemoveAt(dirs.Count - 1);
                Console.WriteLine("Reading " + curdir);
                dirs.AddRange(Directory.GetDirectories(curdir));

                foreach (var filename in Directory.GetFiles(curdir, "*.jpg"))
                {
                    files.Add(new Item
                    {
                        src = new FileInfo(filename),
                        dst = new FileInfo(filename.Replace(srcdir, targetdir)),
                        action = Action.None
                    });
                }
            }

            Console.WriteLine("Generating actions");

            for (int i = files.Count - 1; i >= 0; --i)
            {
                if (files[i].dst.Exists) // no overwrites
                    files[i].action = Action.None;

                else if (files[i].src.Length < 500 * 1024 && files[i].src.Length > 60 * 1024) // less than 500 K and > 60 K
                    files[i].action = Action.Copy;

                else
                    files[i].action = Action.Convert;
            }

            var toCopy = files.Where(x => x.action == Action.Copy).ToArray();
            Console.WriteLine("Copying photos");
            for (int i = toCopy.Length - 1; i >= 0; --i)
            {
                Console.WriteLine("[copy] " + (i.ToString()).PadRight(10, ' ') + "   " + snipstr(toCopy[i].src.FullName));
                if (!toCopy[i].dst.Directory.Exists)
                    toCopy[i].dst.Directory.Create();
                File.Copy(toCopy[i].src.FullName, toCopy[i].dst.FullName, false);
            }

            var toConvert = files.Where(x => x.action == Action.Convert).ToArray();
            if (toConvert.Length > 0)
            {
                ImageCodecInfo jgpEncoder = GetEncoder(ImageFormat.Jpeg);
                System.Drawing.Imaging.Encoder myEncoder = System.Drawing.Imaging.Encoder.Quality;
                EncoderParameters myEncoderParameters = new EncoderParameters(1);
                EncoderParameter myEncoderParameter = new EncoderParameter(myEncoder, 70L);
                myEncoderParameters.Param[0] = myEncoderParameter;

                ParallelOptions po = new ParallelOptions() {
                    MaxDegreeOfParallelism = 2//Environment.ProcessorCount
                };
                HashSet<Item> xitems = new HashSet<Item>(toConvert);
                Parallel.ForEach(toConvert, po, (item, pstate, index) =>
                {
                    lock (xitems)
                    {
                        xitems.Remove(item);
                        Console.WriteLine("[convert] " + (xitems.Count.ToString()).PadRight(10, ' ') + "   " + snipstr(item.src.FullName));
                    }

                    if (!item.dst.Directory.Exists)
                        item.dst.Directory.Create();

                    try
                    {
                        using (var bmp = Bitmap.FromFile(item.src.FullName))
                        {
                            double maxLen = 1600;
                            double ratio = maxLen / ((double)Math.Max(bmp.Width, bmp.Height));
                            int w = (int)(ratio * (double)bmp.Width);
                            int h = (int)(ratio * (double)bmp.Height);

                            var PIs = bmp.PropertyItems;

                            using (var scaledbmp = new Bitmap(w, h))
                            using (var g = Graphics.FromImage(scaledbmp))
                            {
                                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
                                g.DrawImage(bmp, 0, 0, w, h);
                                bmp.Dispose();

                                foreach (var pi in PIs)
                                    scaledbmp.SetPropertyItem(pi);

                                lock (jgpEncoder)
                                {
                                    scaledbmp.Save(item.dst.FullName, jgpEncoder, myEncoderParameters);
                                }
                                g.Dispose();
                                scaledbmp.Dispose();
                            }
                        }
                    }
                    catch (Exception x)
                    {
                        lock (toConvert)
                        {
                            var fc = Console.ForegroundColor;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine(x.ToString());
                            Console.ForegroundColor = fc;
                            Console.ReadKey();
                        }
                    }

                });
            }
        }
        
        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        static string snipstr(string str)
        {
            var len = Console.BufferWidth - 30;
            if (str.Length < len) return str;
            return str.Substring(str.Length - len);
        }

    }
}
